#pragma once

#include <Windows.h>
#include <string>

/**
 * \brief Awesome log class that creates a log file in a given path and appends log messages to it.
 */
class Logger
{
public:
	explicit Logger(std::wstring file_path);
	~Logger();

	Logger(Logger&) = delete;
	Logger& operator=(Logger& other) = delete;

	Logger(Logger&&) = default;
	Logger& operator=(Logger&& other) = default;

	/**
	 * \brief Append a log to the log file.
	 * \param log_message Log message to write.
	 */
	void log(std::wstring log_message);

private:
	static HANDLE _s_open_log_file(std::wstring file_path);

	HANDLE m_log_file;
};