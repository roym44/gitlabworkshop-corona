#include "Common/Common.hpp"
#include "Logger/Logger.hpp"

int main()
{
    std::cout << ("start logging!") << std::endl;

    Logger naor_logger(L"C:\\temp\\log.txt");
    naor_logger.log(L"This is a debug log");

    std::cout << ("stop logging!") << std::endl;
    return 0;
}