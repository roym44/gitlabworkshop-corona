import sys
import subprocess
import traceback

### Tests

def mytest():
	return True

TESTS = (mytest, )

#### MiniSystem

def run_tests():
	total_result = True

	for test in TESTS:
		print("Running {} ...".format(test.__name__), end=" ")

		try:
			result = test()
		except:
			traceback.print_exc()
			result = False
		

		total_result &= result
	
		print("Success" if result else "Failure")

	return total_result



def main():	
	if not run_tests():
		sys.exit(1)

	sys.exit(0)

if __name__ == "__main__":
	main()